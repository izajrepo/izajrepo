#!/usr/bin/python3
import sys
import string


def main():
    fileInput = ("mbox-short.txt")
    calculateWords(fileInput)


def calculateWords(fileInput):
    number_of_words = 0
    with open(fileInput, 'r') as f:
        for line in f:
            words = line.split()
            d = dict()
            for word in words:
                t = word.translate(str.maketrans('', '', string.punctuation)).upper()
                if (len(t) > 0):
                    d[t] = d.get(t, 0) + 1
            print(d)
            l = list()
            for key, value in d.items():
                l.append((value, key))

            l = sorted(l, reverse=True)
            for v, k in l:
                print(k, v)

        # number_of_words += len(words)

        # print("\nwords:", number_of_words)


if _name_ == "_main_":
    main()